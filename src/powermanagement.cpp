#include "freertos/FreeRTOS.h"
#include "esp_pm.h"

#include <SerialDebug.h>

void pmEnable()
{
	esp_pm_config_esp32_t pm;
	pm.light_sleep_enable = pdTRUE;
	pm.max_freq_mhz = 240;
	pm.min_freq_mhz = 80;

	esp_err_t rc = esp_pm_configure(&pm);
	switch(rc) {
		case ESP_OK:
			printlnA("Powermanagement enabled");
			break;
		case ESP_ERR_INVALID_ARG:
			printlnA("Powermanagement has invalid args");
			break;
		case ESP_ERR_NOT_SUPPORTED:
			printlnA("Powermanagement is not supported");
			break;
		default:
			debugA("Powermanagement has returned %d", rc);
			break;
	}
}
