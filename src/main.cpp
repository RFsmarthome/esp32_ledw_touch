#include <Arduino.h>
#include <ArduinoJson.h>

#include <WiFi.h>
#include <WebServer.h>
#include <AutoConnect.h>
#include <ESPmDNS.h>
#include <SPI.h>
#include <SPIFFS.h>

//#include <AsyncMqttClient.h>

#include <SerialDebug.h>

#include "HTTPUpdateServer.h"


#include "driver/ledc.h"
#include "driver/touch_pad.h"

#include "powermanagement.h"
#include "com.h"
#include "mqtt.h"
#include "touch.h"
#include "pwm.h"


#define CHANNELS 2
#define TEMP_DELAY 60

Touch *touch;
Com *comQueue;

auto ledPwm = new Pwm[CHANNELS];

const char thingName[] = "esp32";
const char thingApPasswort[] = "start1234";

String mqttPublishBase("rfcentral");
String mqttServer;
String mqttPort;

const char *taskName ="flash";
WebServer webServer;
AutoConnect portal(webServer);
HTTPUpdateServer httpUpdateServer;
AutoConnectAux update("/update", "Update");

static const char AUX_mqtt_setting[] PROGMEM = R"raw(
	[
		{
			"title": "MQTT Settings",
			"uri": "/mqtt",
			"menu": "true",
			"element": [
				{
					"name": "caption",
					"type": "ACText",
					"value": "MQTT Settings page"
				},
				{
					"name": "mqttserver",
					"type": "ACInput",
					"value": "",
					"label": "Server",
					"placeholder": "MQTT broker server"
				},
				{
					"name": "mqttport",
					"type": "ACInput",
					"value": "",
					"label": "Port",
					"placeholder": "MQTT Port of the broker server"
				},
				{
					"name": "save",
					"type": "ACSubmit",
					"value": "Save",
					"uri": "/mqtt_save"
				}
			]
		},
		{
			"title": "MQTT Settings saved",
			"uri": "/mqtt_save",
			"menu": "false",
			"element": [
				{
					"name": "caption",
					"type": "ACText",
					"value": "Parameters saved"
				}
			]
		}
	]
)raw";



void getParameters(AutoConnectAux &aux) {
	mqttServer = aux["mqttserver"].value;
	mqttServer.trim();

	mqttPort = aux["mqttport"].value;
	mqttPort.trim();
}

String loadParameters(AutoConnectAux &aux, PageArgument &args)
{
	File param = SPIFFS.open("/param.json", "r");
	if(param) {
		if(aux.loadElement(param)) {
			getParameters(aux);
			printlnA("Parameters loaded");
		} else {
			printlnA("Error by loading parameters");
		}
		param.close();
	} else {
		printlnA("Error by opening parameter file");
	}

	return String("");
}

String saveParameters(AutoConnectAux &aux, PageArgument &args)
{
	AutoConnectAux &mqttSettings = *portal.aux(portal.where());
	getParameters(mqttSettings);

	File param = SPIFFS.open("/param.json", "w");
	mqttSettings.saveElement(param, {"mqttserver", "mqttport"});
	param.close();

	return String("");
}

void handleRoot() {
  String  content =
    "<html>"
    "<head>"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
    "</head>"
    "<body>"
    "<div class=\"menu\">" AUTOCONNECT_LINK(BAR_32) "</div>"
	"<p>Build git short HASH "
	GIT_SHORT_HASH
	"</p>"
	"<p>Semantic version: "
	GIT_TAG_VERSION
	"</p>"
	"<p>type: esp32_ledw_touch</p>"
    "</body>"
    "</html>";

  WebServer &webServer = portal.host();
  webServer.send(200, "text/html", content);
}

void initPwm() {
	ledPwm[0].setTimer(LEDC_TIMER_0);
	ledPwm[1].setTimer(LEDC_TIMER_1);

	ledPwm[0].initTimer();
	ledPwm[1].initTimer();

	ledPwm[0].init(18, 100, LEDC_CHANNEL_0);
	ledPwm[1].init(19, 100, LEDC_CHANNEL_1);

	ledc_fade_func_install(0);
}

void taskFlash(void *pvParameters) {
	printlnI("Running flash task");
	for(;;) {
		dim_t cdim;
		bool rc = comQueue->receive(&cdim, portMAX_DELAY);
		printlnD("flash task wake up");
		if(rc && cdim.channel>=0 && cdim.channel<CHANNELS) {
			if(cdim.has_dim) {
				printlnD("Receive DIM command");
				debugV("Set Channel DIM: %d (%d)", cdim.channel, cdim.dim);
				ledPwm[cdim.channel].setValue(cdim.dim);
				mqttSendDIM(cdim.channel, cdim.dim);
			}
			if(cdim.has_gamma) {
				printlnD("Receive GAMMA command");
				debugV("Set Channel Gamma: %d (%f)", cdim.channel, cdim.gamma);
				ledPwm[cdim.channel].setGamma(cdim.gamma);
				ledPwm[cdim.channel].setValue();
			}
		}
	}
}

void setup() {
	delay(1000);
	Serial.begin(115200);
	SPIFFS.begin(true);

	initPwm();
	ledPwm[0].setValue(40);

	const touch_pad_t t[] = {TOUCH_PAD_NUM2, TOUCH_PAD_NUM0, TOUCH_PAD_NUM5, TOUCH_PAD_NUM6, TOUCH_PAD_NUM8, TOUCH_PAD_NUM9};
	touch = new Touch(6, t);
	touch->init();

	printlnA("Start rfcentral...");
	WiFi.enableIpV6();
	String macAddress = WiFi.macAddress();
	macAddress.toLowerCase();
	macAddress.replace(":","");
	String hostname = "esp-"+ macAddress;
	AutoConnectConfig autoConnectConfig; 
	autoConnectConfig.hostName = hostname;
	autoConnectConfig.apid = hostname;
	autoConnectConfig.autoReconnect = true;
	autoConnectConfig.portalTimeout = 60000;
	autoConnectConfig.ticker = false;
	autoConnectConfig.apid = hostname;
	autoConnectConfig.psk = "start1234";
	autoConnectConfig.autoRise = true;
	autoConnectConfig.retainPortal = true;
	autoConnectConfig.autoReset = true;
	autoConnectConfig.autoReconnect = true;
	if(portal.load(FPSTR(AUX_mqtt_setting))) {
		printlnA("Load AUX forms");
		AutoConnectAux &mqttSettings = *portal.aux("/mqtt");
		PageArgument args;
		loadParameters(mqttSettings, args);

		portal.on("/mqtt", loadParameters);
		portal.on("/mqtt_save", saveParameters);
	}
	portal.config(autoConnectConfig);
	httpUpdateServer.setup(&webServer);
	portal.join({update});
	if(portal.begin()) {
		debugA("Wifi connected: %s", WiFi.localIP().toString().c_str());
		MDNS.begin(hostname.c_str());
		MDNS.addService("http", "tcp", 80);
	} else {
		printlnA("Timeout for captive portal. Restart ESP");
		ESP.restart();
		delay(1000);
	}

	ledPwm[0].setValue(20);

	comQueue = new Com();
	mqttSetup(hostname, mqttServer, mqttPort, mqttPublishBase, comQueue, touch);

	WebServer &webServer = portal.host();
	webServer.on("/", handleRoot);

	// Disabled for debuging MQTT
	BaseType_t rc = xTaskCreate(taskFlash, (const char*)"flash", configMINIMAL_STACK_SIZE+4096, NULL, tskIDLE_PRIORITY, NULL);
	if(rc==pdPASS) {
		printlnA("pdPASS by xTaskCreate flash");
	}

	touch->start();
}

void loop() {
	debugHandle();
	portal.handleClient();

	for(int i=0; i<2; ++i) {
		if(touch->isTouched(i)) {
			mqttSendTouched(i, true);
			if(ledPwm[i].getValue()>0) {
				ledPwm[i].setValue(0);
				mqttSendDIM(i, 0);
			} else {
				ledPwm[i].setValue(100);
				mqttSendDIM(i, 100);
			}
		}
	}

	taskYIELD();
}
