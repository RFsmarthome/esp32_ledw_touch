#include <Arduino.h>

#include "driver/touch_pad.h"
#include "freertos/task.h"

#include <SerialDebug.h>

#include "touch.h"

Touch::Touch(int count, const touch_pad_t *num)
{
    m_count = count;

    m_interfaces = new touch_interface_t*;

    for(int i=0; i<m_count; ++i) {
        m_interfaces[i] = new touch_interface_t;
        m_interfaces[i]->num = num[i];
        m_interfaces[i]->baseline = 0;
        m_interfaces[i]->touched = false;
    }
}

Touch::~Touch()
{
    if(m_interfaces) {
        for(int i=0; i<m_count; ++i) {
            delete m_interfaces[i];
        }
        delete[] m_interfaces;
    }
}

void Touch::init()
{
	touch_pad_init();

	for(int i=0; i<m_count; ++i) {
		touch_pad_config(m_interfaces[i]->num, 0);
	}

	touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);

	touch_pad_filter_start(10);

    // Cala base line as avg from 3
	vTaskDelay(50 / portTICK_PERIOD_MS);
	uint16_t touch_value;
    uint32_t base[m_count];
    for(int i=0; i<m_count; ++i) {
        base[i] = 0;
    }

    for(int a=0; a<3; ++a) {
	    for(int i=0; i<m_count; ++i) {
    		touch_pad_read_raw_data(m_interfaces[i]->num, &touch_value);
            base[i] += touch_value;
	    }
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }

    for(int i=0; i<m_count; ++i) {
        m_interfaces[i]->baseline = base[i] / 3;
        m_interfaces[i]->lastState = false;
        m_interfaces[i]->touched = false;
        touch_pad_set_thresh(m_interfaces[i]->num, m_interfaces[i]->baseline * TOUCHPAD_TOUCH_THRESHOLD_PERCENT);
    }
}

void Touch::calcBase(int id)
{
    uint16_t touch_value;

    uint32_t base = 0;
    for(int a=0; a<3; ++a) {
   	    touch_pad_read_raw_data(m_interfaces[id]->num, &touch_value);
        base += touch_value;
        vTaskDelay(50 / portTICK_PERIOD_MS);
    }
    m_interfaces[id]->baseline = base / 3;
}

void Touch::start()
{
	BaseType_t rc = xTaskCreate(task, (const char*)"touch", configMINIMAL_STACK_SIZE+4096, this, tskIDLE_PRIORITY, &m_taskHandle);
	if(rc==pdPASS) {
		printlnA("pdPASS by xTaskCreate touch");
        m_commandQueue = xQueueCreate(m_count, sizeof(int));
	}
}

void Touch::stop()
{
    if(m_taskHandle) {
        vTaskDelete(m_taskHandle);
        m_taskHandle = 0;
    }
    if(m_commandQueue) {
        vQueueDelete(m_commandQueue);
        m_commandQueue = 0;
    }
}

bool Touch::setBase(int id)
{
    if(m_commandQueue) {
        if(xQueueSend(m_commandQueue, &id, 50 & portTICK_PERIOD_MS)) {
            return true;
        }
    }
    return false;
}

bool Touch::isTouched(int id)
{
    bool rc = false;
    if(id < m_count) {
        rc = m_interfaces[id]->touched;
        m_interfaces[id]->touched = false;
    }
    return rc;
}

void Touch::task(void *pvParameters)
{
    int show = 0;
    Touch *touch = (Touch*)pvParameters;
    for(;;) {
		uint16_t value;
		uint16_t value_filterd;

		for(int i=0; i<2; ++i) {
            touch_interface_t *interface = touch->m_interfaces[i];
			touch_pad_read_raw_data(interface->num, &value);
			touch_pad_read_filtered(interface->num, &value_filterd);

			//debugV("Touch (%d): %d / %d [%d]", i, value, value_filterd, value_filterd < (touch->m_baseline[i] * TOUCHPAD_TOUCH_THRESHOLD_PERCENT));

            if(value_filterd < (interface->baseline * TOUCHPAD_TOUCH_THRESHOLD_PERCENT)) {
                interface->lastState = true;
            } else {
                if(interface->lastState) {
                    interface->touched = true;
                }
                interface->lastState = false;
            }

            float noise = value * TOUCHPAD_NOISE_THRESHOLD_PERCENT;
            if((value_filterd < (interface->baseline + noise)) && (value_filterd > (interface->baseline - noise))) {
                interface->baseline = value_filterd;
                touch_pad_set_thresh(interface->num, value_filterd * TOUCHPAD_TOUCH_THRESHOLD_PERCENT);
            }
		}
        if(++show > 100) {
            show = 0;
            debugV("T1 (%d) (%d)", touch->m_interfaces[0]->touched, touch->m_interfaces[1]->touched);
        }

        int id;
        if(xQueueReceive(touch->m_commandQueue, &id, (TickType_t)0)) {
            debugD("Recalculate base for id: %d", id);
            touch->calcBase(id);
        }
		vTaskDelay(pdMS_TO_TICKS(100)); // 10ms
    }
}
