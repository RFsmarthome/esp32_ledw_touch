#include <Arduino.h>

#include "driver/ledc.h"

#include <SerialDebug.h>

#include "pwm.h"

#define PWM_TIMER_MODE LEDC_HIGH_SPEED_MODE
#define PWM_TIMER_BIT LEDC_TIMER_8_BIT
#define PWM_TIMER_MAX 255

Pwm::Pwm()
{
}

Pwm::Pwm(uint8_t gpio, uint16_t res, ledc_channel_t channel, ledc_timer_t timer)
{
	setTimer(timer);
	initTimer();
    init(gpio, res, channel);
}

void Pwm::init(uint8_t gpio, uint16_t res, ledc_channel_t channel)
{
	m_gpio = gpio;
	m_channel = channel;
	m_resolution = res;

    debugA("PGIO %d, RESOLUTION %d, Channel %d, Timer %d", m_gpio, m_resolution, m_channel, m_timer);
	ledc_channel_config_t channelConfig;
	channelConfig.duty = 0;
	channelConfig.speed_mode = PWM_TIMER_MODE;
	channelConfig.hpoint = 0;
	channelConfig.channel = m_channel;
	channelConfig.gpio_num = m_gpio;
	channelConfig.timer_sel = m_timer;
	ledc_channel_config(&channelConfig);
}

void Pwm::setTimer(ledc_timer_t timer)
{
	debugA("Set Timer %d", timer);
	m_timer = timer;
}

void Pwm::initTimer()
{
	debugA("Init timer %d", m_timer);
    ledc_timer_config_t timerConfig;
    timerConfig.duty_resolution = PWM_TIMER_BIT;
   	timerConfig.freq_hz = 1000;
	timerConfig.speed_mode = PWM_TIMER_MODE;
	timerConfig.timer_num = m_timer;
	ledc_timer_config(&timerConfig);
}


uint16_t Pwm::correctGamma(uint16_t value)
{
	return (pow((float)value / (float)m_resolution, m_gamma) * PWM_TIMER_MAX + 0.5);
}

void Pwm::setGamma(float gamma)
{
    m_gamma = gamma;
}

void Pwm::setValue()
{
    setValue(m_value);
}

void Pwm::setValue(uint16_t value)
{
    m_value = value;
	uint16_t v = correctGamma(value);
	debugV("Value: %d (%d)", value, v);
	
	//ledc_set_duty_and_update(PWM_TIMER_MODE, m_channel, v, 0);
	ledc_set_duty(PWM_TIMER_MODE, m_channel, v);
	ledc_update_duty(PWM_TIMER_MODE, m_channel);
}

uint8_t Pwm::getGamma() const
{
    return m_gamma;
}

uint16_t Pwm::getValue() const
{
    return m_value;
}
