#ifndef _COM_H
#define _COM_H

#include <Arduino.h>
#include <freertos/queue.h>

struct dim_t {
	uint8_t channel;
	uint8_t has_gamma:1;
	uint8_t has_dim:1;
	uint8_t dim;
	float gamma;
};

class Com {
public:
	Com();
	virtual ~Com();

	bool send(struct dim_t *d, TickType_t wait);
	bool receive(struct dim_t *d, TickType_t wait);

private:
	QueueHandle_t m_queue;
};

#endif
