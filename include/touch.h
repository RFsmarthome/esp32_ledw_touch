#ifndef _TOUCH_H
#define _TOUCH_H

#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/touch_pad.h"

#define TOUCHPAD_FILTER_IDLE_PERIOD                 100     /**< Period of IIR filter in ms when sensor is not touched. */
#define TOUCHPAD_FILTER_TOUCH_PERIOD                10      /**< Period of IIR filter in ms when sensor is being touched. Shouldn't change this value. */
#define TOUCHPAD_STATE_SWITCH_DEBOUNCE              20      /**< 20ms; Debounce threshold. */
#define TOUCHPAD_BASELINE_RESET_COUNT_THRESHOLD     5       /**< 5 count number; All channels; */
#define TOUCHPAD_BASELINE_UPDATE_COUNT_THRESHOLD    800     /**< 800ms; Baseline update cycle. */
#define TOUCHPAD_TOUCH_LOW_SENSE_THRESHOLD          0.03    /**< 3% ; Set the low sensitivity threshold. When less than this threshold, remove the jitter processing. */
#define TOUCHPAD_TOUCH_THRESHOLD_PERCENT            0.75    /**< 75%; This is button type triggering threshold, should be larger than noise threshold. The threshold determines the sensitivity of the touch. */
#define TOUCHPAD_NOISE_THRESHOLD_PERCENT            0.20    /**< 20%; The threshold is used to determine whether to update the baseline. The touch system has a signal-to-noise ratio of at least 5:1. */
#define TOUCHPAD_HYSTERESIS_THRESHOLD_PERCENT       0.10    /**< 10%; The threshold prevents frequent triggering. */
#define TOUCHPAD_BASELINE_RESET_THRESHOLD_PERCENT   0.20    /**< 20%; If the touch data exceed this threshold for 'RESET_COUNT_THRESHOLD' times, then reset baseline to raw data. */
#define TOUCHPAD_SLIDER_TRIGGER_THRESHOLD_PERCENT   0.50    /**< 50%; This is slider type triggering threshold, should large than noise threshold. when diff-value exceeded this threshold, a sliding operation has occurred. */

struct touch_interface_t {
    touch_pad_t num;
    uint16_t baseline;

    bool lastState:1;
    bool touched:1;
};

class Touch {
public:
    Touch(int count, const touch_pad_t *num);
    virtual ~Touch();

    void init();
    void start();
    void stop();

    bool setBase(int id);
    bool isTouched(int id);

private:
    void calcBase(int id);
    static void task(void *pvParameters);

    int m_count;
    touch_interface_t **m_interfaces = 0;

    TaskHandle_t m_taskHandle = 0;
    QueueHandle_t m_commandQueue = 0;
};

#endif
