#ifndef _PWM_H
#define _PWM_H

#include "driver/ledc.h"

class Pwm
{
public:
    Pwm();
    Pwm(uint8_t gpio, uint16_t res, ledc_channel_t channel, ledc_timer_t);

    void setTimer(ledc_timer_t timer);

    void init(uint8_t gpio, uint16_t res, ledc_channel_t channel);
    void initTimer();

    void setGamma(float gamma);
    void setValue(uint16_t value);
    void setValue();

    uint8_t getGamma() const;
    uint16_t getValue() const;

protected:
    uint16_t correctGamma(uint16_t value);

private:
    ledc_timer_t m_timer;
    ledc_channel_t m_channel;
    uint8_t m_gpio;

    uint16_t m_resolution;
    float m_gamma = 2.2f;
    int m_value = 0;
};

#endif
