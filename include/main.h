#ifndef _MAIN_H
#define _MAIN_H

#include <Arduino.h>

extern void setGamma(uint8_t channel, float gamma);

#endif
