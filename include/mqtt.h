#ifndef _MQTT_H
#define _MQTT_H

class Com;
class Touch;

void mqttSetup(String &hostname, String &server, String &port, String &basePath, Com *com, Touch *touch);

bool mqttSendDIM(uint8_t channel, uint8_t dim);
bool mqttSendDalas(uint8_t sensor, float temp);
bool mqttSendTouched(uint8_t sensor, bool touched);

#endif
